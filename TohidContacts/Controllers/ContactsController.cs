﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TohidContacts.Data;
using TohidContacts.Models;
using TohidContacts.ModelViews;

namespace TohidContacts.Controllers
{

    public class ContactsController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly IHostingEnvironment _hostingEnv;


        public ContactsController(ApplicationDbContext db, IHostingEnvironment hostingEnv)
        {
            _db = db;
            _hostingEnv = hostingEnv;
        }
        public IActionResult NewContact()
        {
            NewContactModelView newContactModelView = new();
            newContactModelView.contact = new();
            newContactModelView.categories = _db.Categories.ToList();

            return View(newContactModelView);
        }

        // Get Edit
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var obj = _db.Contacts.Include(contact => contact.Phones).FirstOrDefault(c => c.Id == id);

            if (obj == null)
            {
                return NotFound();
            }

            NewContactModelView contactModelView = new();
            contactModelView.categories = _db.Categories.ToList();
            contactModelView.contact = obj;
            contactModelView.editMode = true;

            return View(contactModelView);
        }


        // Post Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(NewContactModelView obj)
        {

           
            var phones = _db.Phones.Where(phone => phone.ContactId == obj.contact.Id);
            _db.Phones.RemoveRange(phones);
            _db.SaveChanges();


            if (obj.ContactImageFile != null)
            {
                string uniqueFileName = UploadedFile(obj.ContactImageFile);
                obj.contact.Image = uniqueFileName;
            }

            _db.Contacts.Update(obj.contact);
            _db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(NewContactModelView obj)
        {
           
            if (obj.ContactImageFile != null)
            {
                string uniqueFileName = UploadedFile(obj.ContactImageFile);
                obj.contact.Image = uniqueFileName;
            }

            _db.Contacts.Add(obj.contact);
            _db.SaveChanges();
            return RedirectToAction("Index","Home");
        }

        private string UploadedFile(IFormFile ProfileImage)
        {
            string uniqueFileName = null;

            if (ProfileImage != null)
            {
                string uploadsFolder = Path.Combine(_hostingEnv.WebRootPath, "images");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + ProfileImage.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    ProfileImage.CopyTo(fileStream);
                }
            }
            return uniqueFileName;
        }

        public IActionResult Details(int id)
        {
            Contact contact = _db.Contacts.Include(contact => contact.Phones).Include(contact => contact.Category).FirstOrDefault(c => c.Id == id);
            return View(contact);
        }


        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            var obj = _db.Contacts.Find(id);
            if (obj == null)
            {
                return NotFound();
            }

            _db.Contacts.Remove(obj);
            _db.SaveChanges();

            return RedirectToAction("Index", "Home");

        }

    }
}
