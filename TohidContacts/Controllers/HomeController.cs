﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TohidContacts.Data;
using TohidContacts.Models;
using TohidContacts.ModelViews;
using Microsoft.EntityFrameworkCore;

namespace TohidContacts.Controllers
{
    public class HomeController : Controller
    {

        private readonly ApplicationDbContext _db;
       

        public HomeController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            var contacts = _db.Contacts.AsQueryable();
            if (!String.IsNullOrEmpty(HttpContext.Request.Query["searchkey"]))
            {
                var key = HttpContext.Request.Query["searchkey"];
                contacts = contacts.Where(con => con.FirstName.Contains(key) || con.LastName.Contains(key) || con.Company.Contains(key));
            }

            if (!String.IsNullOrEmpty(HttpContext.Request.Query["IsFav"]))
            {
                if (HttpContext.Request.Query["IsFav"] == "true" || HttpContext.Request.Query["IsFav"] == 1)
                {
                    contacts = contacts.Where(con => con.IsFav == true);
                }
            }
            if (!String.IsNullOrEmpty(HttpContext.Request.Query["CategoryId"]))
            {
                var CategoryId = int.Parse(HttpContext.Request.Query["CategoryId"]);
                contacts = contacts.Where(con => con.CategoryId == CategoryId);

            }
            int page = 1;
            if (!String.IsNullOrEmpty(HttpContext.Request.Query["page"]))
            {
                
                 page = int.Parse(HttpContext.Request.Query["page"]);
                
            }
            if(page > 0)
            {
                page--;
            }

            


            HomeViewModel homeViewModel = new();
            homeViewModel.resultsCount = contacts.Count();
            contacts = contacts.Skip(page * 10).Take(10);

            homeViewModel.contacts = contacts.Include(contact => contact.Phones).ToList();

            homeViewModel.categories = _db.Categories.ToList();
            homeViewModel.allContactsCount = _db.Contacts.Count();
            homeViewModel.favsCount = _db.Contacts.Where(con => con.IsFav == true).Count();
            return View(homeViewModel);

        }

        public IActionResult About()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
