﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TohidContacts.Data;
using TohidContacts.Models;
using TohidContacts.ModelViews;

namespace TohidContacts.Controllers
{
    public class SettingsController : Controller
    {
        private readonly ApplicationDbContext _db;
        public SettingsController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
           
            //get categories from db
            List<Category> categories = _db.Categories.ToList();
            SettingsViewModel settingsViewModel = new();
            settingsViewModel.categories = categories;

            return View(settingsViewModel);
        }
    }
}
