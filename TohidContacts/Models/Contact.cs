﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TohidContacts.Models
{
    public class Contact
    {
        [Key]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string Note { get; set; }
        public string Image { get; set; }
        public int? CategoryId { get; set; }
        public Category Category { get; set; }
        public List<Phone> Phones { get; set; } = new List<Phone>();
        public bool IsFav { get; set; } = false;
        
        public DateTime CreateAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
        public bool IsActive { get; set; } = false;










    }
}
