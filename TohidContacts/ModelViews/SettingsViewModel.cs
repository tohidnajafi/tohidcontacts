﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TohidContacts.Models;

namespace TohidContacts.ModelViews
{
    public class SettingsViewModel
    {
        public List<Category> categories { get; set; }
    }
}
