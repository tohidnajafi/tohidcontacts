﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TohidContacts.Models;

namespace TohidContacts.ModelViews
{
    public class HomeViewModel
    {
        public List<Contact> contacts { get; set; }
        public List<Category> categories { get; set; }

        public int resultsCount { get; set; } = 1;
        public int allContactsCount { get; set; } = 1;
        public int favsCount { get; set; } = 1;

    }
}
