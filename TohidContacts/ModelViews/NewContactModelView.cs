﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TohidContacts.Models;

namespace TohidContacts.ModelViews
{
    public class NewContactModelView
    {
        public Contact contact { get; set; } = new();
        public List<Category> categories { get; set; }

        public bool editMode { get; set; } = false;

        public IFormFile ContactImageFile { get; set; }

    }
}
